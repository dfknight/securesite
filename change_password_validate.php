<?php
  include_once('functions.php');
  include_once('db.php');

  secure_session_start();
  if(!is_encrypted()) {
    echo("This operation can only be performed using HTTPS!");
    exit();
  } elseif(!isset($_SESSION['userid'])) {
    header("Location: index.html");
    exit();
  } elseif(!isset($_POST['old_password']) || !isset($_POST['password']) || !isset($_POST['password_confirm'])) {
    echo("Missing fields!");
    exit();
  } elseif($_POST['password'] != $_POST['password_confirm']) {
    echo("New password and confirm password must match!");
    exit();
  } elseif($_POST['password'] == $_POST['old_password']) {
    echo("New password is the same as the old password!");
    exit();
  }

  $old_password = prevent_injection(prevent_xss($_POST['old_password']));
  $password = prevent_injection(prevent_xss($_POST['password']));
  $password_confirm = prevent_injection(prevent_xss($_POST['password_confirm']));

  $username = db_get_username($_SESSION['userid']);

  if(db_validate_credentials($username, $old_password)) {
    change_password($username, $password);

    db_write_glog("Password has been changed for accountid ".$_SESSION['userid']);

    session_destroy();
  } else {
    db_write_glog("Password was not changed for accountid ".$_SESSION['userid'].". Old password did not match the user's current password.");
  }

  header("Location: index.php");
  exit();
?>
