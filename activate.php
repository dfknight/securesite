<?php
	include_once('functions.php');
	include_once('db.php');

	secure_session_start();

	if(!is_encrypted()) {
		echo('This is only possible over HTTPS communication! Click <a href="index.php">here</a> to go back to the main page! <br />');
		exit();
	} elseif(!isset($_GET['username']) || !isset($_GET['acthash'])) {
		echo('Username and/or Activation hash are not set! Click <a href="index.php">here</a> to go back to the main page! <br />');
		exit();
	} elseif(strlen($_GET['acthash']) != 64) {
		echo('Invalid activation hash detected! Click <a href="index.php">here</a> to go back to the main page! <br />');
		exit();
	} elseif(!ctype_alnum($_GET['username'])) {
		echo('Activation hash must be alpha-numeric! Click <a href="index.php">here</a> to go back to the main page! <br />');
		exit();
	} elseif(!ctype_alnum($_GET['username'])) {
		echo('Username must only contain alpha-numeric characters! Click <a href="index.php">here</a> to go back to the main page! <br />');
		exit();
	} else if(strlen($_GET['username']) < 4 || strlen($_GET['password']) > 25) {
		echo('Username length must be between 4 and 25 Click <a href="index.php">here</a> to go back to the main page! <br />');
		exit();
	}

	$username = prevent_injection(prevent_xss($_GET['username']));
	$acthash = prevent_injection(prevent_xss($_GET['acthash']));

	if(db_username_exists($username) && db_validate_acthash($username, $acthash) && !db_get_actdate($username)) {
		if(activate_user($username, $acthash)) {
			db_write_glog('Account with userid '.db_get_userid($username).' has been activated!');
			echo('Your account has been activated. Click <a href="login.php">here</a> to login</a>');
			exit();
		}
	}
	echo('Activation failure! Either hash or username were invalid, or account has already been activated! Click <a href="index.php">here</a> to go back to the main page! <br />');
	exit();
?>
