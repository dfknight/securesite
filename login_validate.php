<?php
	include_once('functions.php');
	include_once('db.php');

	secure_session_start();

	if(!is_encrypted()) {
		echo('You can only visit this website over HTTPS!');
		exit();
	} elseif(!check_csrf_token()) {
		echo('CSRF Attempt detected! Click <a href="login.php">here</a> to login again! <br />');
		db_write_log('Possible CSRF detected in login_validate.php using user='.$_POST['username'].' and pass='.prevent_injection(prevent_xss($_POST['password'])).' <br />');
		exit();
	} elseif(!isset($_POST['username']) || !isset($_POST['password'])) {
		echo('Username and password are not set! Click <a href="login.php">here</a> to try again! <br />');
		exit();
	} else {
		// Check password input
		if(strlen($_POST['password']) < 8 && strlen($_POST['password']) > 32) {
			echo('Password length must be between (and including) 8 and 32! Click <a href="login.php">here</a> to try again! <br />');
			exit();
		} elseif(!ctype_alnum($_POST['password'])) {
			echo('Password can only be alphanumeric (letters and numbers)! Click <a href="login.php">here</a> to try again! <br />');
			exit();
		}
		// Check user input
		if(strlen($_POST['username']) < 4 && strlen($_POST['username']) > 25) {
			echo('Usename length must be between (and including) 4 and 25! Click <a href="login.php">here</a> to try again! <br />');
			exit();
		} elseif(!ctype_alnum($_POST['password'])) {
			echo('Username can only be alphanumeric (letters and numbers)! Click <a href="login.php">here</a> to try again! <br />');
			exit();
		}
	}

	set_csrf_token();

	// First, we must call prevent_XSS and prevent_injection to ensure that the username and password are safe to use
	$username = prevent_xss(prevent_injection($_POST['username']));
	$password = prevent_xss(prevent_injection($_POST['password']));


	if(db_username_exists($username) && db_validate_credentials($username, $password)) {
		secure_session_start();
		$_SESSION['userid']=db_get_userid($username);
		//secure_session_start(db_get_userid($username));
		// Redirect the user to the profile.php page.

		db_write_llog(db_get_userid($username));
		db_write_glog('User with userid '.db_get_userid($username).' has logged in!');
		header('Location: profile.php');
		exit();
	}
	if(db_get_login_attempts($username) < 4) {
		db_set_login_attempts($username);
		echo('Login attempt failed! Invalid username/password! Click <a href="login.php">here</a> to try again! <br />');
		exit();
	}
	$reset_hash=generate_salt();
	$salt=db_get_salt($username);
	$new_pass=substr(generate_salt(), 0, 16);
	$hashed_pass=hash('sha256', $salt.$new_pass);

	db_write_glog('The account with username '.$username.' failed 5 login attempts and has been locked. It can be unlocked by visiting https://'.$_SERVER['HTTP_HOST'].'/unlock.php?username='.$username.'&uhash='.$reset_hash);
	db_reset_password($username,  $reset_hash);

	echo('Your account has been locked! Instructions on how to unlock this account have been sent to registered email.');
	exit();
?>
