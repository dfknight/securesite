<?php
  include_once('functions.php');
  include_once('db.php');

  secure_session_start();
  if(!is_encrypted()) {
    header("Location: index.html");
    exit();
  } elseif(!isset($_SESSION['userid'])) {
    header("Location: index.html");
    exit();
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <link href="css/login.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </head>
  <body>
    <?php include_once('menu.php'); ?>
    <form class="signin-form" method="post" action="change_password_validate.php">
      <input type="hidden" name="csrf_token" value="<?php echo($token) ?>" />
      <div class="login-text form-signin">
        <h2 class="form-heading">Change Password</h2>
        <div class="input-group">
          <span class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></span>
          <input name="old_password" type="password" class="form-control" placeholder="Old Password" aria-describedby="basic-addon1" required>
        </div>
        <div class="input-group">
          <span class="input-group-addon"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></span>
          <input name="password" type="password" class="form-control" placeholder="New Password" aria-describedby="basic-addon1" required>
        </div>
        <div class="input-group">
          <span class="input-group-addon"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></span>
          <input name="password_confirm" type="password" class="form-control" placeholder="New Password" aria-describedby="basic-addon1" required>
        </div>
        <button class="submit-btn btn btn-primary btn-block" type="submit">Sign In</button>
      </div>
    </form>
  </body>
</html>
