<?php
  include_once('functions.php');

  secure_session_start();
  if(!isset($_SESSION['userid'])) {
    header("Location: index.php");
    exit();
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <link href="css/login.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </head>
  <body>
    <?php include_once('menu.php'); ?>
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
          <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title" id="profile_name"></h3>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="images/profile_unknown.jpg" class="img-circle"> </div>
                <div class=" col-md-9 col-lg-9 ">
                  <form method="POST" action="upload_image_validate.php" enctype="multipart/form-data">
                    Pick a file: <input type="file" name="filename" required/> <br />
                    <input type="submit" value="Submit" />
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
