<?php
  include_once('functions.php');
  include_once('db.php');

  secure_session_start();
  if(!is_encrypted()) {
    echo('You must be connected to HTTPS');
    exit();
  } elseif(!isset($_SESSION['userid'])) {
    header('Location: index.html');
    exit();
  }
  $username = db_get_username($_SESSION['userid']);
  $email = db_get_email($username);
  $firstname = db_get_firstname($username);
  $lastname = db_get_lastname($username);
  $lastlogin = db_get_lastlogin($_SESSION['userid']);

  $token=set_csrf_token();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <link href="css/login.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <?php
      include_once('functions.php');
    ?>
  </head>
  <body>
    <?php include_once('menu.php'); ?>
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
          <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title" id="profile_name"></h3>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="images/profile_unknown.jpg" class="img-circle"> </div>
                <div class=" col-md-9 col-lg-9 ">
                  <form method="POST" action="edit_profile_validate.php">
                    <input name="csrf_token" type="hidden" value="<?php echo($token); ?>" />
                    <table class="table table-user-information">
                      <tbody>
                        <tr>
                          <td>Username</td>
                          <td id="profile_username"><?php echo($username); ?></td>
                        </tr>
                        <tr>
                          <td>Email</td>
                          <td id="profile_email"><?php echo($email); ?></td>
                       </tr>
                       <tr>
                          <td>First Name</td>
                          <td id="profile_fname">
                            <div class="input-group">
                              <input name="firstname" type="text" class="form-control" placeholder="First Name" value="<?php echo($firstname); ?>" minlength="3" maxlength="45" pattern=".{3,45}" aria-describedby="sizing-addon1">
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>Last Name</td>
                          <td id="profile_lname">
                            <div class="input-group">
                              <input name="lastname" type="text" class="form-control" placeholder="Last Name" value="<?php echo($lastname); ?>" minlength="3" maxlength="45" pattern=".{3,45}" aria-describedby="sizing-addon1">
                            </div>
                        </tr>
                        <tr>
                          <td>Last Login</td>
                          <td id="profile_llogin"><?php echo($lastlogin); ?></td>
                        </tr>
                      </tbody>
                    </table>
                    <button class="submit-btn btn btn-primary btn-block" type="submit">Edit Profile</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
