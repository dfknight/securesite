<?php
	include_once('functions.php');
	include_once('db.php');

	secure_session_start();
	if(!is_encrypted()) {
		echo('You can only visit this website over HTTPS! <br />');
		exit();
	} elseif(!check_csrf_token()) {
		echo('Attempted CSRF Detected! This attempt has been logged! Click <a href="index.php">here</a> to go back to the main page! <br />');
		exit();
	} elseif(!isset($_SESSION['userid'])) {
		echo('Session has not been set! Are you logged in? Click <a href="login.php">here</a> to log in! <br />');
		exit();
	} elseif(!isset($_POST['firstname']) || !isset($_POST['lastname'])) {
		echo('One of the fields required is missing! Click <a href="edit_profile_validate.php">here</a> to refill the Edit Profile form! <br />');
		exit();
	} else {
		if(strlen($_POST['firstname']) < 3 || strlen($_POST['firstname']) > 45) {
			echo('Firstname must be between 3 and 45 characters! Click <a href="edit_profile_validate.php">here</a> to refill the Edit Profile form! <br />');
			exit();
		}
		if(strlen($_POST['lastname']) < 3 || strlen($_POST['lastname']) > 45) {
			echo('Lastname must be between 3 and 45 characters! Click <a href="edit_profile_validate.php">here</a> to refill the Edit Profile form! <br />');
			exit();
		}
	}

	unset($_SESSION['csrf_token']);
	session_write_close();


	$firstname = prevent_injection(prevent_xss($_POST['firstname']));
	$lastname = prevent_injection(prevent_xss($_POST['lastname']));
	$username = db_get_username($_SESSION['userid']);

	change_userinfo($username, $firstname, $lastname);

	db_write_glog("User Info (First/Last Name) has been changed for accountid ".$_SESSION['userid']);

	header("Location: profile.php");
	exit();
?>
