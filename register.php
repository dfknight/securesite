<?php
	require('functions.php');
	$token = set_csrf_token();
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <link href="css/login.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </head>
  <body>
    <?php include_once('menu.php'); ?>
    <form class="signin-form" method="post" action="register_validate.php">
      <input type="hidden" name="csrf_token" value="<?php echo($token); ?>" />
      <div class="form-signin">
        <h2 class="form-heading">Register</h2>
	<div class="login-text">
          If you already have an account, you can <a href="login.php">login</a> now!"
        </div>
        <div class="input-group">
          <span class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></span>
          <input name="username" type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1" minlength="4" maxlength="25" pattern=".{4,25}" title="Username must be between 4-25 characters (alpha-numeric)!" required>
        </div>
        <div class="input-group">
          <span class="input-group-addon"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></span>
          <input name="password" type="password" class="form-control" placeholder="Password" aria-describedby="basic-addon1" minlength="8" maxlength="32" pattern=".{8,32}" title="Password must be between 8-32 charcters (alpha-numeric)." onchange="form.password_confirm.pattern = this.value;" required>
        </div>
        <div class="input-group">
          <span class="input-group-addon"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></span>
          <input name="password_confirm" type="password" class="form-control" placeholder="Confirm Password" aria-describedby="basic-addon1" minlength="5" maxlength="64" title="Passwords must match." required>
        </div>
        <div class="input-group">
          <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span></span>
          <input name="email" type="email" class="form-control" placeholder="Email" aria-describedby="basic-addon1" required>
        </div>
        <button class="submit-btn btn btn-primary btn-block" type="submit">Register</button>
      </div>
    </form>
  </body>
</html>
