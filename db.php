<?php
	require('config.php');

	function db_connect() {
		$mysqli = new mysqli(MYSQL_HOST, MYSQL_USER, MYSQL_PASS, MYSQL_DB);
		if($mysqli->connect_errno) {
			return False;
		}
		return $mysqli;
	}

	function db_close($mysqli) {
		$mysqli->close();
	}

	function db_get_username($userid) {
		if($mysqli = db_connect()) {
			$stmt = $mysqli->prepare("SELECT username FROM SecureSite.Users WHERE userid = ?");
			if(!$stmt) {
				db_close($mysqli);
				return False;
			}
			$stmt->bind_param("s", $userid);
			$stmt->execute();
			if($stmt->errno) {
				$stmt->close();
				db_close($mysqli);
				return False;
			}
			$stmt->store_result();
			$rows = $stmt->num_rows;
			if($rows != 1) {
				$stmt->close();
				db_close($mysqli);
				return False;
			}
			$stmt->bind_result($username);
			$stmt->fetch();
			$stmt->close();
			db_close($mysqli);
			return $username;
		}
		return False;
	}

	function db_get_userid($username) {
		if($mysqli = db_connect()) {
			$stmt = $mysqli->prepare("SELECT userid FROM SecureSite.Users WHERE username = ?");
			if(!$stmt) {
				db_close($mysqli);
				return False;
			}
			$stmt->bind_param("s", $username);
			$stmt->execute();
			if($stmt->errno) {
				$stmt->close();
				db_close($mysqli);
				return False;
			}
			$stmt->store_result();
			$rows = $stmt->num_rows;
			if($rows != 1) {
				$stmt->close();
				db_close($mysqli);
				return False;
			}
			$stmt->bind_result($userid);
			$stmt->fetch();
			$stmt->close();
			db_close($mysqli);
			return $userid;
		}
		return False;
	}

	function db_get_firstname($username) {
		if($mysqli = db_connect()) {
			$stmt = $mysqli->prepare("SELECT firstname FROM SecureSite.Users WHERE username = ?");
			if(!$stmt) {
				db_close($mysqli);
				return False;
			}
			$stmt->bind_param("s", $username);
			$stmt->execute();
			if($stmt->errno) {
				$stmt->close();
				db_close($mysqli);
				return False;
			}
			$stmt->store_result();
			$rows = $stmt->num_rows;
			if($rows != 1) {
				$stmt->close();
				db_close($mysqli);
				return False;
			}
			$stmt->bind_result($firstname);
			$stmt->fetch();
			$stmt->close();
			db_close($mysqli);
			return $firstname;
		}
		return False;
	}

        function db_get_lastname($username) {
		if($mysqli = db_connect()) {
			$stmt = $mysqli->prepare("SELECT lastname FROM SecureSite.Users WHERE username = ?");
			if(!$stmt) {
				db_close($mysqli);
				return False;
			}
			$stmt->bind_param("s", $username);
			$stmt->execute();
			if($stmt->errno) {
				$stmt->close();
				db_close($mysqli);
				return False;
			}
			$stmt->store_result();
			$rows = $stmt->num_rows;
			if($rows != 1) {
				$stmt->close();
				db_close($mysqli);
				return False;
			}
			$stmt->bind_result($lastname);
			$stmt->fetch();
			$stmt->close();
			db_close($mysqli);
			return $lastname;
		}
		return False;
	}

	function db_get_lastlogin($userid) {
                if($mysqli = db_connect()) {
                        $stmt = $mysqli->prepare("SELECT logindate FROM SecureSite.Login WHERE userid = ? ORDER BY logindate");
                        if(!$stmt) {
                                db_close($mysqli);
                                return False;
                        }
                        $stmt->bind_param("i", $userid);
                        $stmt->execute();
                        if($stmt->errno) {
                                $stmt->close();
                                db_close($mysqli);
                                return False;
                        }
                        $stmt->store_result();
                        $rows = $stmt->num_rows;
                        if($rows>0) {
                                $stmt->close();
                                db_close($mysqli);
                                return False;
                        }
                        $stmt->bind_result($lastlogin);
                        $stmt->fetch();
                        $stmt->close();
                        db_close($mysqli);
			echo("Last Login: ".$lastlogin.$userid);
                        return $lastlogin;
                }
                return False;
        }

	function db_get_email($username) {
		if($mysqli = db_connect()) {
			$stmt = $mysqli->prepare("SELECT email FROM SecureSite.Users WHERE username = ?");
			if(!$stmt) {
				db_close($mysqli);
				return False;
			}
			$stmt->bind_param("s", $username);
			$stmt->execute();
			if($stmt->errno) {
				$stmt->close();
				db_close($mysqli);
				return False;
			}
			$stmt->store_result();
			$rows = $stmt->num_rows;
			if($rows != 1) {
				$stmt->close();
				db_close($mysqli);
				return False;
			}
			$stmt->bind_result($email);
			$stmt->fetch();
			$stmt->close();
			db_close($mysqli);
			return $email;
		}
		return False;
	}

	function db_get_login_attempts($username) {
		if($mysqli = db_connect()) {
			$stmt = $mysqli->prepare("SELECT login_attempts FROM SecureSite.Users WHERE username = ?");
			if(!$stmt) {
				db_close($mysqli);
				return False;
			}
			$stmt->bind_param("s", $username);
			$stmt->execute();
			if($stmt->errno) {
				$stmt->close();
                                db_close($mysqli);
				return False;
                        }
                        $stmt->store_result();
                        $rows = $stmt->num_rows;
                        if($rows != 1) {
                                $stmt->close();
                                db_close($mysqli);
                                return False;
                        }
                        $stmt->bind_result($login_attempts);
                        $stmt->fetch();
                        $stmt->close();
                        db_close($mysqli);
                        return $login_attempts;
                }
                return False;
	}

	function db_set_login_attempts($username) {
                if($mysqli = db_connect()) {
                        $stmt = $mysqli->prepare("UPDATE SecureSite.Users SET login_attempts = login_attempts +1 WHERE username = ?");
                        if(!$stmt) {
                                db_close($mysqli);
                                return False;
                        }
                        $stmt->bind_param("s", $username);
                        $stmt->execute();
                        if($stmt->errno) {
                                $stmt->close();
                                db_close($mysqli);
                                return False;
                        }
                        $stmt->store_result();
                        $rows = $stmt->num_rows;
                        if($rows != 1) {
                                $stmt->close();
                                db_close($mysqli);
                                return False;
                        }
                        $stmt->bind_result($login_attempts);
                        $stmt->fetch();
                        $stmt->close();
			db_close($mysqli);
                        return $login_attempts;
                }
                return False;
        }

	function db_get_actdate($username) {
		if($mysqli = db_connect()) {
			$stmt = $mysqli->prepare("SELECT actdate FROM SecureSite.Users WHERE username = ?");
			if(!$stmt) {
				db_close($mysqli);
				return False;
			}
			$stmt->bind_param("s", $username);
			$stmt->execute();
			if($stmt->errno) {
				$stmt->close();
				db_close($mysqli);
				return False;
			}
			$stmt->store_result();
			$rows = $stmt->num_rows;
			if($rows != 1) {
				$stmt->close();
				db_close($mysqli);
				return False;
			}
			$stmt->bind_result($actdate);
			$stmt->fetch();
			$stmt->close();
			db_close($mysqli);
			return $actdate;
		}
		return False;
	}


	function db_username_exists($username) {
		if($mysqli = db_connect()) {
			$stmt = $mysqli->prepare("SELECT username FROM SecureSite.Users WHERE username = ?");
			if(!$stmt) {
				db_close($mysqli);
				return False;
			}
			$stmt->bind_param("s", $username);
			$stmt->execute();
			if($stmt->errno) {
				$stmt->close();
				db_close($mysqli);
				return False;
			}
			$stmt->store_result();
			$rows = $stmt->num_rows;
			$stmt->close();
			db_close($mysqli);
			if($rows == 1) {
				return True;
			}
		}
		return False;
	}

	function db_email_exists($email) {
		if($mysqli = db_connect()) {
			$stmt = $mysqli->prepare("SELECT email FROM SecureSite.Users WHERE email = ?");
			if(!$stmt) {
				db_close($mysqli);
				return False;
			}
			$stmt->bind_param("s", $email);
			$stmt->execute();
			if($stmt->errno) {
				$stmt->close();
				db_close($mysqli);
				return False;
			}
			$stmt->store_result();
			$rows = $stmt->num_rows;
			$stmt->close();
			db_close($mysqli);
			if($rows>0) {
				return True;
			}
		}
		return False;
	}

	function db_get_salt($username) {
		if($mysqli = db_connect()) {
			$stmt = $mysqli->prepare("SELECT salt FROM SecureSite.Users WHERE username = ?");
			if(!$stmt) {
				db_close($mysqli);
				return False;
			}
			$stmt->bind_param("s", $username);
			$stmt->execute();
			if($stmt->errno) {
				$stmt->close();
				db_close($mysqli);
				return False;
			}
			$stmt->store_result();
			$rows = $stmt->num_rows;
			if($rows != 1) {
				$stmt->close();
				db_close($mysqli);
				return False;
			}
			$stmt->bind_result($salt);
			$stmt->fetch();
			$stmt->close();
			db_close($mysqli);
			return $salt;
		}
		return False;
	}

	function db_get_avatar($username) {
		if($mysqli = db_connect()) {
			$stmt = $mysqli->prepare("SELECT avatar FROM SecureSite.Users WHERE username = ?");
			if(!$stmt) {
				db_close($mysqli);
				return False;
			}
			$stmt->bind_param("s", $username);
			$stmt->execute();
			if($stmt->errno) {
				$stmt->close();
				db_close($mysqli);
				return False;
			}
			$stmt->store_result();
			$rows = $stmt->num_rows;
			if($rows != 1) {
				$stmt->close();
				db_close($mysqli);
				return False;
			}
			$stmt->bind_result($avatar);
			$stmt->fetch();
			$stmt->close();
			db_close($mysqli);
			return $avatar;
		}
		return False;
	}

	function db_validate_credentials($username, $password) {
		if($mysqli = db_connect()) {
			$salt = db_get_salt($username);
			$password_hash = hash('sha256', $salt.$password);
			$stmt = $mysqli->prepare("SELECT username FROM SecureSite.Users WHERE username = ? AND password = ? AND actdate IS NOT NULL");
			if(!$stmt) {
				db_close($mysqli);
				return False;
			}
			$stmt->bind_param("ss", $username, $password_hash);
			$stmt->execute();
			if($stmt->errno) {
				$stmt->close();
				db_close($mysqli);
				return False;
			}
			$stmt->store_result();
			$rows = $stmt->num_rows;
			$stmt->close();
			db_close($mysqli);
			if($rows == 1) {
				return True;
			}
		}
		return False;
	}

	function db_validate_acthash($username, $acthash) {
		$userid = db_get_userid($username);
		$salt = db_get_salt($username);
		if($acthash == hash('sha256', $salt.$userid)) {
			return True;
		}
		return False;
	}

	function db_store_sessionid($userid, $sessionid) {
		if($mysqli = db_connect()) {
			$stmt = $mysqli->prepare("INSERT INTO SecureSite.Login(userid,logindate,sessionid) VALUES (?, NOW(), ?)");
			if(!$stmt) {
				db_close($mysqli);
				return False;
			}
			$stmt->bind_param("ss", $userid, $sessionid);
			$stmt->execute();
			if($stmt->errno) {
				$stmt->close();
				db_close($mysqli);
				return False;
			}
			$stmt->close();
			db_close($mysqli);
			return True;
		}
		return False;
	}

	function create_user($username,$password,$email) {
		if($mysqli = db_connect()) {
			$stmt = $mysqli->prepare("INSERT INTO SecureSite.Users(username,password,email,regdate,salt) VALUES (?, ?, ?, NOW(), ?)");
			if(!$stmt) {
				db_close($mysqli);
				return False;
			}
			$salt = generate_salt();
			$password_hash=hash('sha256', $salt.$password);
			$stmt->bind_param("ssss", $username, $password_hash, $email, $salt);
			$stmt->execute();
			if($stmt->errno) {
				$stmt->close();
				db_close($mysqli);
				return False;
			}
			$stmt->close();
			db_close($mysqli);

			$acthash=hash('sha256', db_get_salt($username).db_get_userid($username));

			return True;
		}
		return False;
	}

	function db_write_glog($message) {
		if($mysqli = db_connect()) {
			$stmt = $mysqli->prepare("INSERT INTO SecureSite.GeneralLog(message,logdate) VALUES (?, NOW())");
			if(!$stmt) {
				db_close($mysqli);
				return False;
			}
			$stmt->bind_param("s", $message);
			$stmt->execute();
			if($stmt->errno) {
				$stmt->close();
				db_close($mysqli);
				return False;
			}
			$stmt->close();
			db_close($mysqli);
			return True;
		}
		return False;
	}

	function db_write_llog($userid) {
		if($mysqli = db_connect()) {
			$stmt = $mysqli->prepare("INSERT INTO SecureSite.LoginLog(userid,logindate) VALUES (?, NOW())");
			if(!$stmt) {
				db_close($mysqli);
				return False;
			}
			$stmt->bind_param("s",$userid);
			$stmt->execute();
			if($stmt->errno) {
				$stmt->close();
				db_close($mysqli);
				return False;
			}
			$stmt->close();
			db_close($mysqli);
			return True;
		}
		return False;
	}

	function activate_user($username, $acthash) {
		if($acthash == hash('sha256', db_get_salt($username).db_get_userid($username))) {
			if($mysqli = db_connect()) {
				$stmt = $mysqli->prepare("UPDATE SecureSite.Users SET actdate = NOW() WHERE username = ?");
				if(!$stmt) {
					db_close($mysqli);
					return False;
				}
				$stmt->bind_param("s",$username);
				$stmt->execute();
				if($stmt->errno) {
					$stmt->close();
					db_close($mysqli);
					return False;
				}
				$stmt->close();
				db_close($mysqli);
				return True;
			}
		}
		return False;
	}

	function change_password($username, $password) {
		if($mysqli = db_connect()) {
			$stmt = $mysqli->prepare("UPDATE SecureSite.Users SET password = ? WHERE username = ?");
			if(!$stmt) {
				db_close($mysqli);
				return False;
			}
			$hashed_password = hash('sha256', db_get_salt($username).$password);
			$stmt->bind_param("ss", $hashed_password, $username);
			$stmt->execute();
			if($stmt->errno) {
				$stmt->close();
				db_close($mysqli);
				return False;
			}
			$stmt->close();
			db_close($mysqli);
			return True;
		}
		return False;
	}

	function change_userinfo($username, $firstname, $lastname) {
		if($mysqli = db_connect()) {
			$stmt = $mysqli->prepare("UPDATE SecureSite.Users SET firstname = ?, lastname = ? WHERE username = ?");
			if(!$stmt) {
				db_close($mysqli);
				return False;
			}
			$stmt->bind_param("sss", $firstname, $lastname, $username);
			$stmt->execute();
			if($stmt->errno) {
				$stmt->close();
				db_close($mysqli);
				return False;
			}
			$stmt->close();
			db_close($mysqli);
			return True;
		}
		return False;
	}

	function db_update_avatar($username, $location) {
		if($mysqli = db_connect()) {
			$stmt = $mysqli->prepare("UPDATE SecureSite.Users SET avatar = ? WHERE username = ?");
			if(!$stmt) {
				db_close($mysqli);
				return False;
			}
			$stmt->bind_param("ss", $location, $username);
			$stmt->execute();
			if($stmt->errno) {
				$stmt->close();
				db_close($mysqli);
				return False;
			}
			$stmt->close();
			db_close($mysqli);
			return True;
		}
		return False;
	}

	function reset_password($username, $password, $hash) {
		 if($mysqli = db_connect()) {
                        $stmt = $mysqli->prepare("UPDATE SecureSite.Users SET reset_code = ? WHERE username = ?");
                        if(!$stmt) {
                                db_close($mysqli);
                                return False;
                        }
                        $stmt->bind_param("ss", $password, $username);
                        $stmt->execute();
                        if($stmt->errno) {
                                $stmt->close();
                                db_close($mysqli);
                                return False;
                        }
                        $stmt->close();
                        db_close($mysqli);
                        return True;
                }
                return False;
	}

	function db_set_reset_hash($username, $reset_hash) {
		if($mysqli = db_connect()) {
			$stmt = $mysqli->prepare("UPDATE SecureSite.Users SET reset_code = ? WHERE username = ?");
			if(!$stmt) {
				db_close($mysqli);
				return False;
			}
			$stmt->bind_param("ss", $reset_hash, $username);
			$stmt->execute();
			if($stmt->errno) {
				$stmt->close();
				db_close($mysqli);
				return False;
			}
			$stmt->close();
			db_close($mysqli);
			return True;
		}
		return False;
	}

	// Generate a 32-byte (128-bit) salt that will be prepended to the password.
	function generate_salt() {
		do {
			$salt = bin2hex(openssl_random_pseudo_bytes(16, $is_strong));
		} while(!$is_strong);
		return $salt;
	}
?>
