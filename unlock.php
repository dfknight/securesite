<?php
	include_once('functions.php');
	include_once('db.php');

	secure_session_start();

	if(!is_encrypted()) {
		echo('You can only view this website using HTTPS!');
		exit();
	} elseif(!isset($_GET['username']) || !isset($_GET['uhash'])) {
		echo('One or more of the required fields is missing! Click <a href="index.php">here</a> to go back to the main page! <br />');
		exit();
	}

	$username=prevent_xss(prevent_injection($_GET['username']));
	$uhash=prevent_xss(prevent_inject($_GET['uhash']));

	if(db_username_exists($username) && db_account_locked($username) && db_hash_matches($username, $uhash)) {
		db_account_unlock($username);
		echo('Your account has been unlocked now. Click <a href="login.php">here</a> to login and change your password from the temporary one to a new one! <br />');
		exit();
	}
	echo('Your account could not be unlocked. Either it does not exist or the required reset code did not match! <br />Click <a href="index.php">here</a> to go to the main page! <br />');
	exit();
?>
