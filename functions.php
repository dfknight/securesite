<?php
	include_once('db.php');

	// Ensure that communication is done using HTTPS
	// Obtained from: http://stackoverflow.com/questions/1175096/how-to-find-out-if-you-are-using-https-without-serverhttps
	function is_encrypted() {
		if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
			return True;
		}
		return False;
	}

	// Ensure that data is not prone to XSS.
	function prevent_xss($data) {
		return htmlspecialchars($data, ENT_QUOTES, 'UTF-8');
	}

	// Sanitization here to prevent injection
	function prevent_injection($data) {
		return filter_var($data, FILTER_SANITIZE_STRING);
	}

	// Set the CSRF token.
	// Obtained from: http://stackoverflow.com/questions/1780687/preventing-csrf-in-php
	function set_csrf_token() {
		secure_session_start();
		unset($_SESSION['csrf_token']);
		do {
			$token = base64_encode(openssl_random_pseudo_bytes(128, $is_strong));
		} while (!$is_strong);
		$_SESSION['csrf_token'] = $token;
		session_write_close();
		return $token;
	}

	// Check if CSRF token matches, and unset it.
	// Obtained from: http://stackoverflow.com/questions/1780687/preventing-csrf-in-php
	function check_csrf_token() {
		secure_session_start();
		if(isset($_SESSION['csrf_token'])) {
			$token = $_SESSION['csrf_token'];
			unset($_SESSION['csrf_token']);
			if($token == $_POST['csrf_token']) {
				return True;
			}
		}
		session_write_close();
		return False;
	}

	// Start a secure session
	function secure_session_start() {
		// Generate Session
		if(session_status() == PHP_SESSION_NONE) {
			ini_set('session.cookie_httponly', 1);
			ini_set('session.use_only_cookies', 1);
			$cookie_params = session_get_cookie_params();
			session_set_cookie_params($cookie_params['lifetime'], $cookie_params['path'], $cookie_params['domain'], True, True);
			session_name("login_session");
			session_start();
			session_regenerate_id(True);
		}
	}

	// Check to see if a user is logged in or not.
	function login_check() {
		if(isset($_SESSION['userid'])) {
			$userid = $_SESSION['userid'];
			$sessionid = $_SESSION['sessionid'];

			if(db_sessionid_exists($sessionid, $userid)) {
				return True;
			}
		}
		return False;
	}
?>
