<?php
        require('functions.php');
        $token = set_csrf_token();
?>
<!DOCTYPE html>
<html lang="en">
	<head>
    		<link href="css/login.css" rel="stylesheet">
    		<link href="css/bootstrap.min.css" rel="stylesheet">
    		<link href="css/bootstrap-theme.min.css" rel="stylesheet">
    		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min$
    		<script src="js/bootstrap.min.js"></script>
  	</head>
  	<body>
		<?php include_once('menu.php'); ?>
		<form class="signin-form" method="post" action="forgotten_validate.php">
      			<input type="hidden" name="csrf_token" value="<?php echo($token) ?>" />
      			<div class="login-text form-signin">
        			<h2 class="form-heading">Forgotten Password</h2>
        			<div>
          				Enter your email. A newly generated password will be sent to your email.
        			</div>
        			<div class="input-group">
          				<span class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></span>
          				<input name="email" type="email" class="form-control" placeholder="Email" aria-describedby="basic-addon1" required>
        			</div>
        			<button class="submit-btn btn btn-primary btn-block" type="submit">Submit</button>
      			</div>
    		</form>
  	</body>
</html>

