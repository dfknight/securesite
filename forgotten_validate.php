<?php
        include_once('functions.php');
        include_once('db.php');

        secure_session_start();

        if(!is_encrypted()) {
                echo(''You can only visit this website over HTTPS!');
                exit();
        } elseif(!check_csrf_token()) {
                // Possible CSRF Detected
                echo('CSRF Attempt detected! Click <a href="forgotten.php">here</a> to try again! <br />');
		db_write_glog('CSRF Attempt detected on forgotten_validate.php using email='.prevent_injection(prevent_xss($_POST['email'])).'!');
                exit();
        } elseif(!isset($_POST['email'])) {
                echo('One of the required fields is missing! Click <a href="forgotten.php">here</a> to try again! <br />);
                exit();
        }

	 // First, we must call prevent_XSS and prevent_Injection to ensure that the data received is safe to use
        $email = prevent_xss(prevent_injection($_POST['email']));
	echo($email);
        if(!db_email_exists($username)) {
		echo('Email does not exist in the database! Click <a href="forgotten.php">here</a> to try again! <br />');
		exit();
        }

	//Generate a random password
	$password = substr(generate_salt(), 0, 8);
       	db_write_glog('Your password has been changed. your new password is '.$password.'. Go to: '$_SERVER['HTTP_HOST'].'/login.php to login using your new password.');

	change_password($username, $password);

	echo('Your password has been changed. Please check your email with your new password, and you can login <a href="login.php">here<a/>!');
	exit();
?>
