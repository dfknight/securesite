<?php
	include_once('functions.php');
	include_once('db.php');

	secure_session_start();

	if(!is_encrypted()) {
		echo('You can only visit this website over HTTPS!');
		exit();
	} elseif(!check_csrf_token()) {
		// Possible CSRF Detected
		echo('CSRF Attempt detected! Click <a href="register.php">here</a> to register again! <br />');
		db_write_log('Possible CSRF detected in login_validate.php using user='.$_POST['username'].' and pass='.prevent_injection(prevent_xss($_POST['password'])).' <br />');
		exit();
	} elseif(!isset($_POST['username']) || !isset($_POST['password']) || !isset($_POST['password_confirm']) || !isset($_POST['email'])) {
		echo('One of the required fields is missing! Click <a href="register.php">here</a> to register again! <br />');
		exit();
	} elseif($_POST['password'] !== $_POST['password_confirm']) {
		echo('Passwords must match! Click <a href="register.php">here</a> to try again! <br />');
		exit();
	} else {
		if(strlen($_POST['username']) < 4 || strlen($_POST['username']) > 25) {
			echo('Username length must be between (and including) 4 and 25! Click <a href="register.php">here</a> to try again!');
			exit();
		} elseif(!ctype_alnum($_POST['username'])) {
			echo('Username can only be alphanumeric (letters and numbers)! Click <a href="register.php">here</a> to try again! <br />');
			exit();
		}

		if(strlen($_POST['password']) < 8 || strlen($_POST['password']) > 32) {
			echo('Password length must be between (and including) 8 and 32! Click <a href="register.php">here</a> to try again! <br />');
			exit();
		} elseif(!ctype_alnum($_POST['password'])) {
			echo('Password can only be alphanumeric (letters and numbers)! Click <a href="register.php">here</a> to try again! <br />');
			exit();
		}

		if(strlen($_POST['email']) < 8 || strlen($_POST['email']) > 64) {
			echo('Email length must be between 5 and 64! Click <a href="regsiter.php">here</a> to try again! <br />');
			exit();
		} elseif(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
			echo('Invalid e-mail format! Click <a href="regsiter.php">here</a> to try again! <br />');
			exit();
		}
	}

	set_csrf_token();

	// First, we must call prevent_XSS and prevent_Injection to ensure that the data received is safe to use
	$username = prevent_xss(prevent_injection($_POST['username']));
	$password = prevent_xss(prevent_injection($_POST['password']));
	$email = prevent_xss(prevent_injection($_POST['email']));

	if(db_username_exists($username)) {
		echo('Username already exists! Click <a href="regsiter.php">here</a> to try again! <br />');
		exit();
	} elseif(db_email_exists($email)) {
		echo('Email already exists! Click <a href="regsiter.php">here</a> to try again! <br />');
		exit();
	}


	create_user($username, $password, $email);
	$acthash = hash('sha256', db_get_salt($username).db_get_userid($username));
	db_write_glog('Account has been created for userid '.db_get_userid($username).'. It can be activated at: '.$_SERVER['HTTP_HOST'].'/activate.php?username='.$username.'&acthash='.$acthash);

	echo('Your account has been activated. It can be activated using the link provided in the email that was recently sent to you. <br />Click <a href="index.php">here</a> to go back to the index page!<br />');

	// Time to call the db functions.

	// Check if username and password matches
	// This is done using credentials on the database

	// If matches, redirect to profile page

	// If no match, then show an error
	exit();
?>
