<?php
	include_once('functions.php');
	include_once('db.php');
        secure_session_start();
	if(isset($_SESSION['userid'])) {
		db_write_glog("User with userid ".$_SESSION['userid']." has logged out!");
	}
	unset($_SESSION['csrf_token']);
	session_destroy();
	header("Location: index.php");
	exit();
?>
