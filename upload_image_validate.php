<?php
	include_once('functions.php');
	include_once('db.php');

	secure_session_start();

	if(!is_encrypted()) {
		echo("This operationg can only be performed on HTTPS.");
		exit();
	} elseif (!check_csrf_token()) {
		echo('CSRF Attempt detected! Click <a href="login.php">here</a> to login again! <br />');
		db_write_log('Possible CSRF detected in login_validate.php using user='.$_POST['username'].' and pass='.prevent_injection(prevent_xss($_POST['password'])).' <br />');
		exit();
	}

	if(getimagesize($_FILES['filename']['tmp_name'])) {
		$location="images/";
		$location.= bin2hex(openssl_random_pseudo_bytes(8));
		if(!file_exists($location)) {
			$username=db_get_username($_SESSION['userid']);
			move_uploaded_file($_FILES['filename']['tmp_name'], $location);
			if(db_update_avatar($username, $location)) {
				db_write_glog("Avatar updated to ".$location."  for userid ".$_SESSION['userid'].".");
			} else {
				db_write_glog("Inserting avatar path to db failed!");
			}
			header("Location: profile.php");
			exit();
		} else {
			db_write_glog("File already exists!");
		}
	} else {
		db_write_glog("Unable to obtain image size for file ".$_FILES['filename']['name']." OR ".$_FILES['filename']['tmp_name']);
	}

	db_write_glog("Unable to update avatar for userid ".$_SESSION['userid'].".");

	header("Location: profile.php");
	exit();
?>
