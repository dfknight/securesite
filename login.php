<?php
	require('functions.php');
	$token = set_csrf_token();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <link href="css/login.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </head>
  <body>
    <?php include_once('menu.php'); ?>
    <form class="signin-form" method="post" action="login_validate.php">
      <input type="hidden" name="csrf_token" value="<?php echo($token) ?>" />
      <div class="login-text form-signin">
        <h2 class="form-heading">Login</h2>
        <div>
          If you don't have account, you can <a href="register.html">create</a> one now!
        </div>
        <div class="input-group">
          <span class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></span>
          <input name="username" type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1" minlength="4" maxlength="25" autocomplete="off" pattern=".{4,25}" title="Username must be between 4-25 characters (alpha-numeric)!" required>
        </div>
        <div class="input-group">
          <span class="input-group-addon"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></span>
          <input name="password" type="password" class="form-control" placeholder="Password" aria-describedby="basic-addon1" minlength="8" maxlength="32" autocomplete="off" pattern=".{8,32}" title="Password must be between 8-32 characters (alpha-numeric)!" required>
        </div>
	<div>
		<a class="links" style="float: right;" href="#">Forgot your password?</a>
	</div>
        <button class="submit-btn btn btn-primary btn-block" type="submit">Sign In</button>
      </div>
    </form>
  </body>
</html>
