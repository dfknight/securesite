<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="index.php">Secure Site</a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <?php
          include_once('functions.php');
          secure_session_start();
          if(isset($_SESSION['userid'])) {
            echo('<li><a href="profile.php">Profile</a></li>');
            echo('<li><a href="logout.php">Logout</a></li>');
          } else {
            echo('<li><a href="login.php">Login</a></li>');
            echo('<li><a href="register.php">Register</a></li>');
          }
        ?>
      </ul>
    </div>
  </div>
</nav>
