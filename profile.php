<?php
  include_once('functions.php');
  include_once('db.php');

  secure_session_start();
  if(!isset($_SESSION['userid'])) {
    header('Location: index.php');
    exit();
  }
  $username = db_get_username($_SESSION['userid']);
  $email = db_get_email($username);
  $firstname = db_get_firstname($username);
  $lastname = db_get_lastname($username);
  $lastlogin = db_get_lastlogin($_SESSION['userid']);
  $avatar = db_get_avatar($username);
  if(!$avatar) {
    $avatar='images/profile_unknown.jpg';
  }

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <link href="css/login.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </head>
  <body>
    <?php include_once('menu.php'); ?>
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
          <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title" id="profile_name"></h3>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src='<?php echo($avatar); ?>' class="img-circle"> </div>
                <div class=" col-md-9 col-lg-9 ">
                  <table class="table table-user-information">
                    <tbody>
                      <tr>
                        <td>Username</td>
                        <td id="profile_username"><?php echo($username); ?></td>
                      </tr>
                      <tr>
                        <td>Email</td>
                        <td id="profile_email"><?php echo($email); ?></td>
                      </tr>
                      <tr>
                        <td>First Name</td>
                        <td id="profile_fname"><?php echo($firstname); ?></td>
                      </tr>
                      <tr>
                        <td>Last Name</td>
                        <td id="profile_lname"><?php echo($lastname); ?></td>
                      </tr>
                      <tr>
                        <td>Last Login</td>
                        <td id="profile_llogin"><?php echo($lastlogin); ?></td>
                      </tr>
                    </tbody>
                  </table>
                  <a href="edit_profile.php" class="btn btn-primary">Edit Profile</a>
                  <a href="change_password.php" class="btn btn-primary">Change Password</a>
                  <a href="upload_image.php" class="btn btn-primary">Upload Image</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
